import Vue from "vue";
import VueRouter from "vue-router";
import MatchPlanning from "../views/MatchPlanning";
import Results from "../views/Results";
import store from "../store/index";
import ConductMatchComponent from "../dialogs/ConductMatchDialog";
import PartialRoundRobinView from "../views/PartialRoundRobinView";
import MatchView from "../dialogs/MatchOverviewDialog";
import ResultTeamView from "../dialogs/ResultTeamView";
import SetupComponent from "../views/SetupComponent";
import EditCompetitorsComponent from "../dialogs/TeamEditingDialog";
import ManualMatchCreationDialog from "../dialogs/ManualMatchCreationDialog";
import VisualisationSettingsDialog from "../dialogs/VisualisationSettingsDialog";
import AlgorithmSettingsDialog from "../dialogs/AlgorithmSettingsDialog";
import MatchPlanningSettingsDialog from "../dialogs/MatchPlanningSettingsDialog";
import TournamentHandlingDialog from "../dialogs/TournamentHandlingDialog";
import AboutDialog from "../dialogs/AboutDialog";

Vue.use(VueRouter);

const routes = [
  {
    path: "/schedules",
    name: "home",
    component: MatchPlanning,
    meta: {
      needsSetup: true,
      isHomeScreen: true
    },
    children: [
      {
        path: "/schedules/teams",
        component: EditCompetitorsComponent,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/schedules/about",
        name: "about",
        component: AboutDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/schedules/tournamentHandling",
        name: "tournamentHandling_home",
        component: TournamentHandlingDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/schedules/matchresult/:id",
        name: "matchresult",
        component: ConductMatchComponent,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/schedules/createfixture",
        name: "manualmatchcreation",
        component: ManualMatchCreationDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/schedules/matchplanningsettings",
        name: "matchplanningsettings",
        component: MatchPlanningSettingsDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      }
    ]
  },
  {
    path: "/setup",
    component: SetupComponent,
    meta: {
      onlyForSetup: true,
      isHomeScreen: true
    },
    children: [
      {
        path: "/setup/teams",
        component: EditCompetitorsComponent,
        meta: {
          onlyForSetup: true,
          isHomeScreen: true
        }
      }
    ]
  },
  {
    path: "/results",
    name: "results",
    component: Results,
    meta: {
      needsSetup: true,
      isHomeScreen: true
    },
    children: [
      {
        path: "/tournamentHandling",
        name: "tournamentHandling_results",
        component: TournamentHandlingDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/results/matches",
        name: "matches",
        component: MatchView,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/results/team/:id",
        name: "teamView",
        component: ResultTeamView,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      }
    ]
  },
  {
    path: "/prrs",
    name: "prrs",
    component: PartialRoundRobinView,
    meta: {
      needsSetup: true,
      isHomeScreen: true
    },
    children: [
      {
        path: "/tournamentHandling",
        name: "tournamentHandling_prrs",
        component: TournamentHandlingDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/prrs/pastsettings",
        name: "pastsettings",
        component: VisualisationSettingsDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      },
      {
        path: "/prrs/settings",
        name: "algorithmSetting",
        component: AlgorithmSettingsDialog,
        meta: {
          needsSetup: true,
          isHomeScreen: true
        }
      }
    ]
  },
  { path: "*", redirect: "/schedules" }
];

var router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.meta.needsSetup && store.getters["settings/inSetupProcess"])
    next("/setup");

  if (to.meta.onlyForSetup && !store.getters["settings/inSetupProcess"])
    next("/schedules");

  if (to.path === "/schedules/createfixture" && from !== "/schedules")
    next("/schedules");

  next();
});
export default router;
