export default function getTransitionDirection(from, to) {
  if (to.path === "/results" && from.path === "/") return "forward";

  if (to.path === "/" && from.path === "/results") return "backword";

  if (to.path === "/settings" && ["/", "/results"].includes(from.path))
    return "forward";

  if (from.path === "/settings" && to.path !== "/teams") return "backword";

  if (to.path === "/teams") {
    return "forward";
  }
  if (from.path === "/teams") return "backword";

  if (to.path === "/matchresult") return "forward";

  if (from.path === "/matchresult") return "backword";
}
