export default class ArrayUtility {
  static getEmptyArrayOfArrays(length) {
    let array = [];
    for (let i = 0; i < length; i++) array.push([]);
    return array;
  }

  static replaceValuesOf2DArray(originalArray, replacementValueDict) {
    let newArray = ArrayUtility.getEmptyArrayOfArrays(originalArray.length);
    for (let i = 0; i < originalArray.length; i++)
      for (let j = 0; j < originalArray[i].length; j++) {
        const replacementValue = replacementValueDict[originalArray[i][j]];
        if (replacementValue != null)
          newArray[i] = newArray[i].concat([replacementValue]);
      }
    return newArray;
  }
}
