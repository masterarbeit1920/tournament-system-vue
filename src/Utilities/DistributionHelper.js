export function getDistributionOfSortedArray(dictionary) {
  var distribution = {};
  for (const key of Object.keys(dictionary)) {
    const amountOfValuesToKey = dictionary[key].length;
    distribution[key] = amountOfValuesToKey;
  }
  return distribution;
}
