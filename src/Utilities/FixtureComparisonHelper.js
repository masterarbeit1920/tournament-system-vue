export function isEqual(one, two) {
  if (one.length !== two.length) return false;
  for (const team of one) {
    if (!two.includes(team)) return false;
  }
  return true;
}

export function includesFixtures(fixtureArray, fixture) {
  for (const fixtureArrayFixture of fixtureArray) {
    if (isEqual(fixtureArrayFixture, fixture)) return true;
  }
  return false;
}
