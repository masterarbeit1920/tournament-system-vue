import { getDistributionOfSortedArray } from "./DistributionHelper";

export function getTeamsSortedForParticipation(fixtureLists) {
  const teamsWithParticipation = {};
  for (const fixtureList of fixtureLists) {
    for (const fixture of fixtureList) {
      for (const teamID of fixture) {
        if (!teamsWithParticipation[teamID]) teamsWithParticipation[teamID] = 0;
        teamsWithParticipation[teamID] += 1;
      }
    }
  }

  const teamsSortedForParticipation = {};
  for (const teamID of Object.keys(teamsWithParticipation)) {
    const participation = teamsWithParticipation[teamID];
    if (!teamsSortedForParticipation[participation])
      teamsSortedForParticipation[participation] = [];
    teamsSortedForParticipation[participation].push(teamID);
  }
  return teamsSortedForParticipation;
}

export function getDistributionOfParticiaption(fixtureLists) {
  const teamsSortedForParticipation = getTeamsSortedForParticipation(
    fixtureLists
  );
  return getDistributionOfSortedArray(teamsSortedForParticipation);
}
