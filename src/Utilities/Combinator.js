export default class Combinator {
  static getCombinations(comboOptions, comboLength) {
    var connections = this.getCombinationsRecursiv(comboOptions, comboLength);
    return connections.map(connection => connection.sort());
  }

  static getCombinationsRecursiv(comboOptions, comboLength) {
    if (comboLength === 1) {
      return comboOptions.map(comboOption => [comboOption]);
    }
    const combos = [];
    Array.prototype.forEach.call(comboOptions, (currentOption, optionIndex) => {
      const smallerCombos = Combinator.getCombinations(
        comboOptions.slice(optionIndex + 1),
        comboLength - 1
      );
      smallerCombos.forEach(smallerCombo => {
        combos.push([currentOption].concat(smallerCombo));
      });
    });
    return combos;
  }
  //source: https://github.com/trekhleb/javascript-algorithms/blob/master/src/algorithms/sets/combinations/combineWithoutRepetitions.js
}
