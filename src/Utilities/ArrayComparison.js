export function isSecondLargerThenFirst(first, second) {
  for (const i in first) {
    if (first[i] < second[i]) return true;
    else if (first[i] > second[i]) return false;
  }
  return false;
}

export function isSecondSmallerThenFirst(first, second) {
  for (const i in first) {
    if (first[i] > second[i]) return true;
    else if (first[i] < second[i]) return false;
  }
  return false;
}
