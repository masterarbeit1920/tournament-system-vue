import Combinator from "./Combinator";
import { getDistributionOfSortedArray } from "./DistributionHelper";

function getConnectionWithActivity(fixtureLists) {
  var connections = [];
  for (const fixtureList of fixtureLists) {
    for (const fixture of fixtureList) {
      connections.push(...Combinator.getCombinations(fixture, 2));
    }
  }
  var connectionsWithActivity = {};
  for (const connection of connections) {
    if (!connectionsWithActivity[connection])
      connectionsWithActivity[connection] = 0;
    connectionsWithActivity[connection] += 1;
  }
  return connectionsWithActivity;
}

export function getConnectionWithActivityAndSortedForActivity(fixtureLists) {
  const connectionsWithActivity = getConnectionWithActivity(fixtureLists);

  var connectionSortedForActivity = {};
  for (const connection of Object.keys(connectionsWithActivity)) {
    const activity = connectionsWithActivity[connection];
    if (!connectionSortedForActivity[activity])
      connectionSortedForActivity[activity] = [];
    connectionSortedForActivity[activity].push(connection);
  }
  return {
    sorted: connectionSortedForActivity,
    withActivity: connectionsWithActivity
  };
}

export function getConnectionsSortedForActivity(fixtureLists) {
  return getConnectionWithActivityAndSortedForActivity(fixtureLists).sorted;
}

export function getDistributionOfConnections(fixtureLists) {
  const connectionsSortedForActivity = getConnectionsSortedForActivity(
    fixtureLists
  );
  return getDistributionOfSortedArray(connectionsSortedForActivity);
}
