export default class DictUitility {
  static getLowestValueKeys(dict) {
    return DictUitility.getKeysSortedForValue(dict)[0];
  }

  static getKeysSortedForValue(dict) {
    let sortedKeys = [[]];
    for (let key in dict) {
      const currentValue = dict[key];
      while (sortedKeys.length <= currentValue) sortedKeys.push([]);
      sortedKeys[currentValue] = sortedKeys[currentValue].concat([key]);
    }
    return sortedKeys.filter(entry => entry.length > 0);
  }

  static getRangeOfValues(dict) {
    const sortedKeys = DictUitility.getKeysSortedForValue(dict);
    const first = dict[sortedKeys[sortedKeys.length - 1][0]];
    const second = dict[sortedKeys[0][0]];
    return first - second;
  }

  static getMinValue(dict) {
    const sortedKeys = DictUitility.getKeysSortedForValue(dict);
    return dict[sortedKeys[0][0]];
  }
}
