import Combinator from "../Utilities/Combinator";
import {
  isSecondLargerThenFirst,
  isSecondSmallerThenFirst
} from "../Utilities/ArrayComparison";
import shuffle from "../Utilities/shuffle";

export default class PPRRCombinator {
  constructor(
    teams,
    teamsPerMatch,
    intensity,
    nextMatchGenerationOption,
    matchComparisonOption,
    knownMatches
  ) {
    this.teams = teams;
    this.teamsPerMatch = teamsPerMatch;
    this.notDoneConnections = Combinator.getCombinations(teams.sort(), 2);
    this.matchCombo = this.initializeMatchCombo(knownMatches);
    this.intensity = intensity;
    this.nextMatchGenerationOption = nextMatchGenerationOption;
    this.matchComparisonOption = matchComparisonOption;
  }

  initializeMatchCombo(knownMatches) {
    var newMatchCombo = [];
    if (knownMatches.length > 0) {
      newMatchCombo.push(...knownMatches);
    } else {
      const firstMatch = this.teams.sort().slice(0, this.teamsPerMatch);
      newMatchCombo.push(firstMatch);
    }
    newMatchCombo.forEach(match => {
      this.removeDoneConnections(match);
    });
    return newMatchCombo;
  }

  removeDoneConnections(match) {
    const doneConnections = Combinator.getCombinations(
      match,
      2
    ).map(connection => connection.toString());
    const newNotDoneConnectionsList = this.notDoneConnections.filter(
      connection => !doneConnections.includes(connection.toString())
    );
    this.notDoneConnections = newNotDoneConnectionsList;
  }

  getCombination() {
    return new Promise(resolve => {
      setTimeout(() => {
        while (this.notDoneConnections.length > 0) {
          const nextMatch = this.getNextMatch();
          this.removeDoneConnections(nextMatch);
          this.matchCombo.push(nextMatch);
        }
        resolve(this.matchCombo);
      }, 0);
    });
  }

  getNextMatch() {
    const connectionWithGlobalActivity = this.getCountedConnectionAsDict();
    const nextConnection = this.getNextConnection();
    var bestDistribution = null;
    var match = null;
    var i = 0;
    var teamsSortedForParticipation = null;
    if (this.nextMatchGenerationOption === "Participation")
      teamsSortedForParticipation = this.getTeamsSortedByParticipation();

    while (i < this.intensity || match == null) {
      let nextMatch = null;
      if (this.nextMatchGenerationOption === "Random")
        nextMatch = this.generateMatch(nextConnection);
      else {
        nextMatch = this.generateMatchWithAttentionOnParticipation(
          nextConnection,
          teamsSortedForParticipation
        );
      }

      const curDistribution = this.getDistribution(
        nextMatch,
        connectionWithGlobalActivity
      );
      if (
        bestDistribution == null ||
        this.isSecondDistributionBetter(bestDistribution, curDistribution)
      ) {
        match = nextMatch;
        bestDistribution = curDistribution;
      }
      i++;
    }
    return match;
  }

  isSecondDistributionBetter(first, second) {
    if (this.matchComparisonOption === "Less_High") {
      const firstCopy = [...first];
      const secondCopy = [...second];
      return isSecondSmallerThenFirst(
        firstCopy.reverse(),
        secondCopy.reverse()
      );
    }
    return isSecondLargerThenFirst(first, second);
  }

  generateMatch(nextConnection) {
    var match = [...nextConnection];
    const suffixOptions = this.teams.filter(
      team => !nextConnection.includes(team)
    );
    match.push(...shuffle(suffixOptions).slice(0, this.teamsPerMatch - 2));
    return match;
  }

  generateMatchWithAttentionOnParticipation(nextConnection, sortedTeams) {
    var additionalTeams = [];
    var i = 0;
    while (additionalTeams.length < this.teamsPerMatch - 2) {
      const teamsOnCurrentLevel = sortedTeams[i].filter(
        team => !nextConnection.includes(team)
      );
      const stillNeededTeams = this.teamsPerMatch - additionalTeams.length - 2;
      additionalTeams.push(
        ...shuffle(teamsOnCurrentLevel).slice(0, stillNeededTeams)
      );
      i++;
    }
    var match = [];
    match.push(...nextConnection);
    match.push(...additionalTeams);
    return match;
  }

  getTeamsSortedByParticipation() {
    var involvedTeams = [];
    involvedTeams.push(...this.teams);
    for (const match of this.matchCombo) involvedTeams.push(...match);
    var teamsWithParticipation = {};
    for (const team of this.teams) teamsWithParticipation[team] = 0;
    for (const team of involvedTeams) teamsWithParticipation[team] += 1;
    var teamsSortedForPariticipation = {};
    for (const team of this.teams) {
      const participation = teamsWithParticipation[team];
      if (!teamsSortedForPariticipation[participation])
        teamsSortedForPariticipation[participation] = [];
      teamsSortedForPariticipation[participation].push(team);
    }
    var teamsSortedForParticipationArray = [];
    for (const number of Object.keys(teamsSortedForPariticipation)
      .map(string => parseInt(string))
      .sort())
      teamsSortedForParticipationArray.push(
        teamsSortedForPariticipation[number]
      );
    return teamsSortedForParticipationArray;
  }

  getCountedConnectionAsDict() {
    var connections = [];
    connections.push(...Combinator.getCombinations(this.teams, 2));
    this.matchCombo.forEach(match => {
      connections.push(...Combinator.getCombinations(match, 2));
    });
    var dictionary = {};
    connections.forEach(connection => {
      if (!Object.keys(dictionary).includes(connection.toString()))
        dictionary[connection] = 0;
      dictionary[connection] += 1;
    });
    return dictionary;
  }

  getNextConnection() {
    return this.notDoneConnections.sort()[0];
  }

  getDistribution(match, connectionWithGlobalActivity) {
    const connections = Combinator.getCombinations(match, 2);
    const frequencies = [];
    connections.forEach(connection => {
      frequencies.push(connectionWithGlobalActivity[connection.toString()]);
    });
    var dictionary = {};
    for (
      let i = 1;
      i <= Math.max(...Object.values(connectionWithGlobalActivity));
      i++
    ) {
      dictionary[i] = 0;
    }
    frequencies.forEach(frequency => {
      dictionary[frequency] += 1;
    });
    return Object.values(dictionary);
  }
}
