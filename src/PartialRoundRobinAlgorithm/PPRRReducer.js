import { getId } from "../Utilities/IdHelper";
import { includesFixtures } from "../Utilities/FixtureComparisonHelper";
import { getConnectionWithActivityAndSortedForActivity } from "../Utilities/FixtureListConnectionHelper";
import Combinator from "../Utilities/Combinator";
import {
  isSecondLargerThenFirst,
  isSecondSmallerThenFirst
} from "../Utilities/ArrayComparison";
import shuffle from "../Utilities/shuffle";

export default class PPRRReducer {
  constructor(
    matchCombo,
    teamIDs,
    blockedMatches,
    intensity,
    fillMethod,
    deletionMethod,
    mergeMethod,
    teamsPerMatch
  ) {
    this.initializeMatchDictionary(matchCombo, blockedMatches);
    this.teamsPerMatch = teamsPerMatch;
    this.intensity = intensity;
    this.fillMethod = fillMethod;
    this.deletionMethod = deletionMethod;
    this.mergeMethod = mergeMethod;
    this.teamIDs = teamIDs;
  }

  initializeMatchDictionary(matchCombo, blockedMatches) {
    this.matches = {};
    this.matchIndicesAllowedToReduce = [];
    for (const match of matchCombo) {
      const id = getId();
      this.matches[id] = match;
      if (!includesFixtures(blockedMatches, match))
        this.matchIndicesAllowedToReduce.push(id);
    }
  }

  getReducesMatchCombination() {
    return new Promise(resolve => {
      setTimeout(() => {
        this.deleteMatchesWithoutSingleConnections();
        this.mergeMatches();
        resolve(Object.values(this.matches));
      }, 0);
    });
  }

  deleteMatchesWithoutSingleConnections() {
    for (;;) {
      this.updateConnectionActivityDict();
      const matchIndicesWithoutSingleConnections = this.getMatchIndicesWithoutSingleConnections();
      if (Object.keys(matchIndicesWithoutSingleConnections).length === 0)
        return;
      if (this.deletionMethod == "Random") {
        const randomMetaIndex = Math.floor(
          Math.random(matchIndicesWithoutSingleConnections.length)
        );
        const indexToDelete =
          matchIndicesWithoutSingleConnections[randomMetaIndex];
        this.deleteMatch(indexToDelete);
      } else {
        let bestIndex = null;
        let bestDistribution = null;
        for (const index of matchIndicesWithoutSingleConnections) {
          const curDistribution = this.getDistributionOfConnectionActivitiesOfMatch(
            this.matches[index]
          );
          if (
            bestDistribution == null ||
            curDistribution.reverse() > bestDistribution.reverse()
          ) {
            bestIndex = index;
            bestDistribution = curDistribution;
          }
        }
        this.deleteMatch(bestIndex);
      }
    }
  }

  deleteMatch(index) {
    this.matchIndicesAllowedToReduce = this.matchIndicesAllowedToReduce.filter(
      id => id !== index
    );
    delete this.matches[index];
  }

  updateConnectionActivityDict() {
    const result = getConnectionWithActivityAndSortedForActivity([
      Object.values(this.matches)
    ]);
    this.connectionsSortedForActitivy = result.sorted;
    this.connectionsWithActivity = result.withActivity;
    this.activities = [];
    for (const activityString of Object.keys(
      this.connectionsSortedForActitivy
    )) {
      const activityNumber = parseInt(activityString);
      if (!this.activities[activityNumber])
        this.activities.push(activityNumber);
    }
  }

  getMatchIndicesWithoutSingleConnections() {
    let indices = [];
    for (const matchIndex of this.matchIndicesAllowedToReduce) {
      const amountOfSingleConnections = this.getAmountOfSingleConnections(
        this.matches[matchIndex]
      );
      if (amountOfSingleConnections === 0) indices.push(matchIndex);
    }
    return indices;
  }

  getAmountOfSingleConnections(match) {
    const connections = Combinator.getCombinations(match, 2);
    let counter = 0;
    for (const connection of connections) {
      const connectionActivity = this.connectionsWithActivity[connection];
      if (connectionActivity === 1) counter++;
    }
    return counter;
  }

  getDistributionOfConnectionActivitiesOfMatch(match) {
    const connections = Combinator.getCombinations(match, 2);
    let array = [];
    for (const connection of connections)
      array.push(this.connectionsWithActivity[connection]);
    let distribution = [];

    for (const activity of this.activities.sort())
      distribution.push(array.filter(number => number === activity).length);
    return distribution;
  }

  mergeMatches() {
    for (;;) {
      this.updateConnectionActivityDict();
      const mergeOptions = this.getMergeOptions();
      if (mergeOptions.length === 0) return;
      const chosenMergeOption = this.choseMergeOption(mergeOptions);
      this.merge(chosenMergeOption);
    }
  }

  merge(option) {
    this.matchIndicesAllowedToReduce = this.matchIndicesAllowedToReduce.filter(
      id => !option.pair.includes(id)
    );
    delete this.matches[option.pair[0]];
    delete this.matches[option.pair[1]];
    const id = getId();
    this.matchIndicesAllowedToReduce.push(id);
    this.matches[id] = option.match;
  }

  choseMergeOption(mergeOptions) {
    let bestMergeOption = null;
    let bestDistribution = null;
    for (const option of mergeOptions) {
      const distribution = this.getDistributionOfConnectionActivitiesOfMatch(
        option.match
      );
      if (
        bestDistribution === null ||
        this.isSecondBetter(bestDistribution, distribution)
      ) {
        bestMergeOption = option;
        bestDistribution = distribution;
      }
    }
    return bestMergeOption;
  }

  isSecondBetter(distOne, distTwo) {
    if (this.mergeMethod === "Less_High") {
      const firstCopy = [...distOne];
      const secondCopy = [...distTwo];
      return isSecondSmallerThenFirst(
        firstCopy.reverse(),
        secondCopy.reverse()
      );
    }
    if (this.mergeMethod === "More_Low") {
      return isSecondLargerThenFirst(distOne, distTwo);
    }
  }

  getMergeOptions() {
    let mergeOptions = [];
    for (const indicePair of this.getRandomIndicePairs()) {
      const mustHaveTeams = this.getMustHaveTeams(indicePair);
      if (mustHaveTeams.length <= this.teamsPerMatch)
        mergeOptions.push({
          pair: indicePair,
          match: this.getAssembeMatch(indicePair, mustHaveTeams)
        });
    }
    return mergeOptions;
  }

  getMustHaveTeams(indicePair) {
    let mustHaveTeams = [];
    let doubleConnections = [{}, {}];
    for (const i in indicePair) {
      const index = indicePair[i];
      const match = this.matches[index];
      for (const connection of Combinator.getCombinations(match, 2)) {
        if (this.connectionsWithActivity[connection.toString()] === 1)
          mustHaveTeams.push(...connection);
        if (this.connectionsWithActivity[connection.toString()] === 2) {
          doubleConnections[i][connection.toString()] = connection;
        }
      }
    }
    for (const connectionString of Object.keys(doubleConnections[0])) {
      if (Object.keys(doubleConnections[1]).includes(connectionString))
        mustHaveTeams.push(...doubleConnections[0][connectionString]);
    }
    return Array.from(new Set(mustHaveTeams));
  }

  getRandomIndicePairs() {
    const amountOfCombinations =
      (this.matchIndicesAllowedToReduce.length *
        (this.matchIndicesAllowedToReduce.length - 1)) /
      2;
    if (amountOfCombinations < this.intensity)
      return Combinator.getCombinations(this.matchIndicesAllowedToReduce, 2);
    else {
      let reference = [];
      let list = [];
      while (list.length < this.intensity) {
        const pair = this.getRandomPair(
          this.matchIndicesAllowedToReduce.length
        );
        if (!reference.includes(pair.toString())) {
          reference.push(pair.toString());
          list.push(pair);
        }
      }
      return list.map(pair => [
        this.matchIndicesAllowedToReduce[pair[0]],
        this.matchIndicesAllowedToReduce[pair[1]]
      ]);
    }
  }

  getRandomPair(maxIndex) {
    const first = Math.floor(Math.random() * maxIndex);
    let second = Math.floor(Math.random() * maxIndex);
    while (first == second) second = Math.floor(Math.random() * maxIndex);
    if (first > second) return [second, first];
    else return [first, second];
  }

  getAssembeMatch(indeces, mustHaveTeams) {
    let mergedMatch = [];
    mergedMatch.push(...mustHaveTeams);
    const openTeamSlots = this.teamsPerMatch - mustHaveTeams.length;
    if (openTeamSlots > 0) {
      const teamsPossibleForSlots = this.teamIDs.filter(
        id => !mustHaveTeams.includes(id)
      );
      if (teamsPossibleForSlots.length === openTeamSlots) {
        mergedMatch.push(...teamsPossibleForSlots);
      } else {
        if (this.fillMethod === "Random")
          mergedMatch.push(
            ...shuffle(teamsPossibleForSlots).slice(0, openTeamSlots)
          );
        if (this.fillMethod === "first")
          mergedMatch.push(...teamsPossibleForSlots.slice(0, openTeamSlots));
        if (this.fillMethod == "Block") {
          const filteredTeamsPossibleForSlots = this.blockTeams(
            teamsPossibleForSlots,
            mergedMatch,
            openTeamSlots
          );
          mergedMatch.push(...filteredTeamsPossibleForSlots);
        }
      }
    }
    return mergedMatch.sort();
  }

  blockTeams(teamsPossibleForSlots, presentTeams, openSlots) {
    let blockedTeams = new Set();
    for (const activity of this.activities.reverse()) {
      for (const connection of this.connectionsSortedForActitivy[activity]) {
        const teams = connection.split(",");
        if (
          !presentTeams.includes(teams[0]) &&
          !presentTeams.includes(teams[1])
        ) {
          if (!blockedTeams.has(teams[1])) blockedTeams.add(teams[0]);
        } else {
          if (
            presentTeams.includes(teams[0]) &&
            !presentTeams.includes(teams[1])
          )
            blockedTeams.add(teams[1]);
          else {
            if (
              presentTeams.includes(teams[1]) &&
              !presentTeams.includes(teams[0])
            )
              blockedTeams.add(teams[0]);
          }
        }
        if (teamsPossibleForSlots.length - blockedTeams.size == openSlots)
          return teamsPossibleForSlots.filter(team => !blockedTeams.has(team));
      }
    }
    return teamsPossibleForSlots
      .filter(team => !blockedTeams.has(team))
      .slice(0, openSlots);
  }
}
