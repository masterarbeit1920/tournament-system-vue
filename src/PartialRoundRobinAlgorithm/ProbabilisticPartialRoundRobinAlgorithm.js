import PPRRCombinator from "./PPRRCombinator";
import PPRRReducer from "./PPRRReducer";
import { includesFixtures } from "../Utilities/FixtureComparisonHelper";

const getCombinationOfMatches = async function(
  teamIDs,
  doneFixtures,
  algorithmSettings,
  teamsPerMatch
) {
  const combinator = new PPRRCombinator(
    teamIDs,
    teamsPerMatch,
    algorithmSettings.combinationIntensity,
    algorithmSettings.nextMatchGenerationOption,
    algorithmSettings.matchComparisonOption,
    doneFixtures
  );

  const combinationPromise = await combinator.getCombination();
  const reducer = new PPRRReducer(
    combinationPromise,
    teamIDs,
    doneFixtures,
    algorithmSettings.reducingIntensity,
    algorithmSettings.reducingMergeMatchFillMethod,
    algorithmSettings.reducingDeletionMethod,
    algorithmSettings.reducingComparisonMethod,
    teamsPerMatch
  );
  const reducesCombination = await reducer.getReducesMatchCombination();
  const notDoneFixtures = reducesCombination.filter(
    fixture => !includesFixtures(doneFixtures, fixture)
  );
  return {
    notDoneFixtures: notDoneFixtures,
    reducedMatchAmount: combinationPromise.length - reducesCombination.length
  };
};

export default getCombinationOfMatches;
