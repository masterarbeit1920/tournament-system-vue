import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import fixtureModule from "./Modules/FixtureStoreModule";
import resultModule from "./Modules/ResultStoreModule";
import teamModule from "./Modules/TeamStoreModule";
import settingsModule from "./Modules/SettingsStoreModule";
import PRRStateModule from "./Modules/PRRStateModule";
import TournamentStoreModule from "./Modules/TournamentStoreModule";

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: "vuex",
  storage: window.localStorage
});

const store = new Vuex.Store({
  plugins: [vuexLocalStorage.plugin],
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    result: resultModule,
    fixtures: fixtureModule,
    teams: teamModule,
    settings: settingsModule,
    prrs: PRRStateModule,
    tournament: TournamentStoreModule
  }
});
export default store;
