import { getId } from "../../Utilities/IdHelper";

const initialState = {
  matchList: [],
  results: {
    allPointAmounts: 0,
    pointsSimple: {},
    averagePoints: {},
    amountOfResults: {}
  }
};

const resultModule = {
  namespaced: true,
  state: initialState,
  getters: {
    matchList(state) {
      return state.matchList;
    },
    results(state) {
      return state.results;
    },
    averagePointAmounts(state) {
      //Needed for trigger the computed property to update when a match was done - results are saved in a nested style
      state.results.allPointAmounts += 0;
      const averagePoints = Array.from(
        new Set(Object.values(state.results.averagePoints))
      );
      return averagePoints
        .sort((a, b) => parseFloat(a) - parseFloat(b))
        .reverse();
    },
    teamsSortedForPointAmount(state, getters) {
      //Needed for trigger the computed property to update when a match was done - results are saved in a nested style
      state.results.allPointAmounts += 0;
      var teamsSortedForPoints = {};
      for (const pointAmount of getters.averagePointAmounts)
        teamsSortedForPoints[pointAmount] = [];

      for (const teamID of Object.keys(state.results.averagePoints)) {
        teamsSortedForPoints[state.results.averagePoints[teamID]].push(teamID);
      }
      return teamsSortedForPoints;
    },
    orderedTeamResults(state, getters) {
      let orderedTeamResults = [];
      for (const i in getters.averagePointAmounts) {
        const pointAmount = getters.averagePointAmounts[i];
        for (const teamId of getters.teamsSortedForPointAmount[pointAmount]) {
          orderedTeamResults.push({
            place: parseInt(i) + 1,
            teamID: teamId,
            points: pointAmount
          });
        }
      }
      return orderedTeamResults;
    },
    areThereResultsToPresent(state) {
      return state.matchList.length > 0;
    }
  },
  mutations: {
    addMatch(state, match) {
      state.matchList.push(match);
    },
    registerPoints(state, payload) {
      if (!state.results.pointsSimple[payload.teamID])
        state.results.pointsSimple[payload.teamID] = 0;
      state.results.pointsSimple[payload.teamID] += payload.points;

      if (!state.results.amountOfResults[payload.teamID])
        state.results.amountOfResults[payload.teamID] = 0;
      state.results.amountOfResults[payload.teamID] += 1;

      if (!state.results.averagePoints[payload.teamID])
        state.results.averagePoints[payload.teamID] = 0;
      state.results.averagePoints[payload.teamID] = (
        state.results.pointsSimple[payload.teamID] /
        state.results.amountOfResults[payload.teamID]
      ).toFixed(2);

      state.results.allPointAmounts += 1;
    },
    resetState(state) {
      state.matchList = [];
      state.results = {
        allPointAmounts: 0,
        pointsSimple: {},
        averagePoints: {},
        amountOfResults: {}
      };
    }
  },
  actions: {
    registerMatchResult({ commit }, matchResult) {
      commit("addMatch", matchResult);
      Object.keys(matchResult).map(teamID => {
        commit("registerPoints", {
          teamID: teamID,
          points: parseInt(matchResult[teamID])
        });
      });
    },
    setState({ dispatch, commit }, matchList) {
      for (const match of matchList) {
        dispatch("registerMatchResult", match);
        const fixtureID = getId();
        commit(
          "fixtures/registerFixture",
          {
            id: fixtureID,
            fixture: Object.keys(match)
          },
          { root: true }
        );
        commit("fixtures/markFixtureAsDone", fixtureID, { root: true });
      }
    }
  }
};

export default resultModule;
