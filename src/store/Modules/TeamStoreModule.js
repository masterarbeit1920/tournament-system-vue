import { getId } from "../../Utilities/IdHelper";
import { names } from "../Names";
import shuffle from "../../Utilities/shuffle";

const initialState = {
  teamIDs: [],
  teams: {}
};

const teamModule = {
  namespaced: true,
  state: initialState,
  getters: {
    teamIDs(state) {
      return state.teamIDs;
    },
    teams(state) {
      return state.teams;
    },
    teamNames(state) {
      //Needed for trigger the computed property to update when a match was done - results are saved in a nested style
      state.teamIDs.push();
      return Object.keys(state.teams).map(id => state.teams[id].name);
    },
    teamAmount: function(state) {
      return state.teamIDs.length;
    },
    isTeamListEmpty: function(state, getters) {
      return getters.teamIDs.length === 0;
    }
  },
  mutations: {
    addParty(state, party) {
      const id = getId();
      state.teams[id] = party;
      state.teamIDs.push(id);
      if (!this.getters["settings/inSetupProcess"]) {
        this.dispatch(
          "fixtures/calculatedMatchCombinations",
          { onlySettingsChanged: false },
          { root: true }
        );
      }
    },
    addParties(state, parties) {
      for (const party of parties) {
        const id = getId();
        state.teams[id] = party;
        state.teamIDs.push(id);
      }
      if (!this.getters["settings/inSetupProcess"])
        this.dispatch(
          "fixtures/calculatedMatchCombinations",
          { onlySettingsChanged: false },
          { root: true }
        );
    },
    editPartyName(state, payload) {
      state.teams[payload.id].name = payload.newName;
    },
    deleteParty(state, idToDelete) {
      state.teamIDs = state.teamIDs.filter(id => id !== idToDelete);
      this.dispatch("settings/adjustTeamsPerMatch", null, { root: true });
    },
    resetState(state) {
      state.teamIDs = [];
      state.teams = {};
    },
    setState(state, teams) {
      for (const teamID of Object.keys(teams)) {
        state.teamIDs.push(teamID);
        state.teams[teamID] = {
          name: teams[teamID].name,
          isActive: teams[teamID].isActive
        };
      }
    }
  },
  actions: {
    fillWithDefaultParties({ getters, commit }) {
      let parties = [];
      const namesTaken = getters["teamNames"];
      let namesToAdd = names.filter(name => !namesTaken.includes(name));
      if (namesToAdd.length > 5) namesToAdd = shuffle(namesToAdd).slice(0, 5);
      for (const name of namesToAdd)
        parties.push({ name: name, isActive: true });
      commit("addParties", parties);
    }
  }
};

export default teamModule;
