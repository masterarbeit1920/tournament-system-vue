import { getId } from "../../Utilities/IdHelper";
import { adjectives } from "../Names";

const initialState = {
  identifier: [],
  fixtureLists: {},
  settings: {},
  lastIdentifier: null,
  visibileDistributions: [],
  showDoneMatchesDistribution: true
};

const PRRStateModule = {
  namespaced: true,
  state: initialState,
  getters: {
    currentIdentifier(state) {
      return state.lastIdentifier;
    },
    showDoneMatchesDistribution(state) {
      return state.showDoneMatchesDistribution;
    },
    IDs(state) {
      return state.identifier;
    },
    fixtureLists(state) {
      const object = {};
      state.identifier.map(id => (object[id] = state.fixtureLists[id]));
      return object;
    },
    settings(state) {
      const object = {};
      state.identifier.map(id => (object[id] = state.settings[id]));
      return object;
    },
    visibileDistributions(state) {
      return state.visibileDistributions;
    }
  },
  mutations: {
    initialize(state) {
      state.identifier = [];
      state.settings = {};
      state.fixtureLists = {};
      state.lastIdentifier = null;
    },
    addEntry(state, payload) {
      state.identifier.push(payload.id);
      state.settings[payload.id] = payload.settings;
      state.fixtureLists[payload.id] = payload.fixtureList;
      state.lastIdentifier = payload.id;
    },
    resetState(state) {
      Object.assign(state, initialState);
    },
    changeVisibilityStatus(state, id) {
      if (state.visibileDistributions.includes(id))
        state.visibileDistributions = state.visibileDistributions.filter(
          idRegistered => id != idRegistered
        );
      else state.visibileDistributions.push(id);
    },
    setAllToVisible(state) {
      state.visibileDistributions.push(...state.identifier);
    },
    setAllToNotVisible(state) {
      state.visibileDistributions = [];
    },
    toggleDoneMatchesDistribution(state) {
      state.showDoneMatchesDistribution = !state.showDoneMatchesDistribution;
    }
  },
  actions: {
    registerFixtureList({ getters, commit, rootGetters }, payload) {
      if (!payload.onlySettingsChanged) commit("initialize");
      const fixtureList = rootGetters["fixtures/fixtures"];
      const settings = rootGetters["settings/algorithmSettings"];
      settings["reduceFixtureAmount"] = payload.reducedFixtureAmount;
      const id = uniqueIdentifier(getters.IDs);
      commit("addEntry", {
        id: id,
        settings: settings,
        fixtureList: fixtureList
      });
    }
  }
};

function uniqueIdentifier(presentIdentifier) {
  if (presentIdentifier.length >= adjectives.length) return getId();
  let id = adjectives[Math.floor(Math.random() * adjectives.length)];
  while (presentIdentifier.includes(id))
    id = adjectives[Math.floor(Math.random() * adjectives.length)];
  return id;
}

export default PRRStateModule;
