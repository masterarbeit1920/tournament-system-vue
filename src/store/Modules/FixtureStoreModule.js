import { getId } from "../../Utilities/IdHelper";
import getCombinationOfMatches from "../../PartialRoundRobinAlgorithm/ProbabilisticPartialRoundRobinAlgorithm";

const initialState = {
  plannedFixtureIDs: [],
  doneFixtureIDs: [],
  fixtures: {},
  tempFixtureIDs: [],
  isCalculating: false
};

const fixtureModule = {
  namespaced: true,
  state: initialState,
  getters: {
    isCalculating(state) {
      return state.isCalculating;
    },
    plannedFixtureIDs(state) {
      return state.plannedFixtureIDs;
    },
    plannedFixtures(state) {
      return state.plannedFixtureIDs.map(id => state.fixtures[id]);
    },
    doneFixtures(state) {
      return state.doneFixtureIDs.map(id => state.fixtures[id]);
    },
    fixtureAmount(state) {
      return state.plannedFixtureIDs.length;
    },
    fixtureDict(state) {
      return state.fixtures;
    },
    fixtures(state, getters) {
      let allFixtures = [];
      allFixtures.push(...getters.plannedFixtures);
      allFixtures.push(...getters.doneFixtures);
      return allFixtures;
    }
  },
  mutations: {
    addFixture(state, fixture) {
      const id = getId();
      state.plannedFixtureIDs.push(id);
      state.fixtures[id] = fixture;
    },
    registerFixture(state, payload) {
      state.fixtures[payload.id] = payload.fixture;
    },
    deleteAllFixtures(state) {
      for (const id of state.plannedFixtureIDs) delete state.fixtures[id];
      state.plannedFixtureIDs = [];
    },
    markFixtureAsDone(state, fixtureID) {
      state.plannedFixtureIDs = state.plannedFixtureIDs.filter(
        id => id !== fixtureID
      );
      state.doneFixtureIDs.push(fixtureID);
    },
    rotateTeamsList(state) {
      const first = state.plannedFixtureIDs[0];
      var temp = state.plannedFixtureIDs.slice(1);
      temp.push(first);
      state.plannedFixtureIDs = temp;
    },
    setLoading(state) {
      state.isCalculating = true;
    },
    stopLoading(state) {
      state.isCalculating = false;
    },
    resetState(state) {
      state.plannedFixtureIDs = [];
      state.doneFixtureIDs = [];
      state.fixtures = {};
      state.tempFixtureIDs = [];
      state.isCalculating = false;
    }
  },
  actions: {
    async calculatedMatchCombinations(
      { getters, dispatch, commit, rootGetters },
      payload
    ) {
      commit("setLoading");
      if (!rootGetters["settings/inSetupProcess"]) {
        commit("deleteAllFixtures");

        const result = await getCombinationOfMatches(
          rootGetters["teams/teamIDs"],
          getters["doneFixtures"],
          rootGetters["settings/algorithmSettings"],
          rootGetters["settings/teamsPerMatch"]
        );
        const matches = result.notDoneFixtures;
        matches.map(fixture => {
          commit("addFixture", fixture);
        });
        dispatch(
          "prrs/registerFixtureList",
          {
            onlySettingsChanged: payload.onlySettingsChanged,
            reducedFixtureAmount: result.reducedMatchAmount
          },
          { root: true }
        );

        /*
        setTimeout(()=>{
          console.log("ready")
          commit('stopLoading')
        },2000);
        */
      }
      commit("stopLoading");
    },
    addTempFixture({ commit }, fixture) {
      const id = getId();
      commit("registerFixture", { fixture: fixture, id: id });
      return id;
    },
    markFixtureAsDone({ getters, commit, dispatch }, fixtureID) {
      const plannedFixtureAmountBefore = getters["fixtureAmount"];
      commit("markFixtureAsDone", fixtureID);
      const plannedFixtureAmountAfter = getters["fixtureAmount"];
      if (plannedFixtureAmountAfter === plannedFixtureAmountBefore)
        dispatch("calculatedMatchCombinations", { onlySettingsChanged: false });
    }
  }
};

export default fixtureModule;
