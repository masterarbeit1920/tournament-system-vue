import { getId } from "../../Utilities/IdHelper";

const tournamentModule = {
  namespaced: true,
  state: {
    tournamentIDs: [],
    tournaments: {}
  },
  getters: {
    tournamentIDs(state) {
      return state.tournamentIDs;
    },
    tournaments(state) {
      return state.tournaments;
    }
  },
  mutations: {
    saveCurrentTournament(state, payload) {
      const id = getId();
      state.tournamentIDs.push(id);
      state.tournaments[id] = payload;
    },
    removeTournament(state, idToDelete) {
      delete state.tournaments[idToDelete];
      state.tournamentIDs = state.tournamentIDs.filter(id => id != idToDelete);
    }
  },
  actions: {
    saveCurrentTournament({ commit, rootGetters }) {
      commit("saveCurrentTournament", {
        settings: rootGetters["settings/tournamentSettings"],
        matches: rootGetters["result/matchList"],
        teams: rootGetters["teams/teams"]
      });
    },
    resetAllStates({ commit }) {
      commit("settings/resetState", null, { root: true });
      commit("result/resetState", null, { root: true });
      commit("fixtures/resetState", null, { root: true });
      commit("teams/resetState", null, { root: true });
      commit("prrs/resetState", null, { root: true });
    },
    loadTournament({ state, commit, dispatch }, id) {
      dispatch("settings/setState", state.tournaments[id].settings, {
        root: true
      });
      commit("teams/setState", state.tournaments[id].teams, { root: true });
      dispatch("result/setState", state.tournaments[id].matches, {
        root: true
      });
      dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: false },
        { root: true }
      );
      commit("removeTournament", id);
    }
  }
};

export default tournamentModule;
