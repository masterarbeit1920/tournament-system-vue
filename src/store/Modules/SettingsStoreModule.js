const initialState = {
  teamsPerMatch: null,
  inSetupProcess: true,
  tournamentName: "",
  isInPresentingMode: true,
  isManualFixtureCreationEnabled: false,
  combinationIntensity: 20,
  reducingIntensity: 20,
  nextMatchGenerationOption: "Random",
  matchComparisonOption: "More_Low",
  reducingDeletionMethod: "Highest",
  reducingComparisonMethod: "More_Low",
  reducingMergeMatchFillMethod: "Random"
};

const settingsModule = {
  namespaced: true,
  state: initialState,
  getters: {
    inSetupProcess(state) {
      return state.inSetupProcess;
    },
    teamsPerMatch(state) {
      return state.teamsPerMatch;
    },
    tournamentName(state) {
      return state.tournamentName;
    },
    isInPresentingMode(state) {
      return state.isInPresentingMode;
    },
    isManualFixtureCreationEnabled(state) {
      return state.isManualFixtureCreationEnabled;
    },
    CombinationIntensity(state) {
      return state.combinationIntensity;
    },
    ReducingIntensity(state) {
      return state.reducingIntensity;
    },
    nextMatchGenerationOption(state) {
      return state.nextMatchGenerationOption;
    },
    matchComparisonOption(state) {
      return state.matchComparisonOption;
    },
    reducingDeletionMethod(state) {
      return state.reducingDeletionMethod;
    },
    reducingComparisonMethod(state) {
      return state.reducingComparisonMethod;
    },
    reducingMergeMatchFillMethod(state) {
      return state.reducingMergeMatchFillMethod;
    },
    algorithmSettings(state) {
      return {
        combinationIntensity: state.combinationIntensity,
        nextMatchGenerationOption: state.nextMatchGenerationOption,
        matchComparisonOption: state.matchComparisonOption,
        reducingIntensity: state.reducingIntensity,
        reducingDeletionMethod: state.reducingDeletionMethod,
        reducingComparisonMethod: state.reducingComparisonMethod,
        reducingMergeMatchFillMethod: state.reducingMergeMatchFillMethod
      };
    },
    tournamentSettings(state) {
      return {
        combinationIntensity: state.combinationIntensity,
        nextMatchGenerationOption: state.nextMatchGenerationOption,
        matchComparisonOption: state.matchComparisonOption,
        reducingIntensity: state.reducingIntensity,
        reducingDeletionMethod: state.reducingDeletionMethod,
        reducingComparisonMethod: state.reducingComparisonMethod,
        reducingMergeMatchFillMethod: state.reducingMergeMatchFillMethod,
        teamsPerMatch: state.teamsPerMatch,
        tournamentName: state.tournamentName
      };
    }
  },
  mutations: {
    decreaseTeamsPerMatchByOne(state) {
      state.teamsPerMatch -= 1;
      if (!state.inSetupProcess)
        this.dispatch(
          "fixtures/calculatedMatchCombinations",
          { onlySettingsChanged: false },
          { root: true }
        );
    },
    changeTeamsPerMatch(state, newValue) {
      state.teamsPerMatch = newValue;
      if (!state.inSetupProcess)
        this.dispatch(
          "fixtures/calculatedMatchCombinations",
          { onlySettingsChanged: false },
          { root: true }
        );
    },
    finishSetupProcess(state, tournamentName) {
      state.tournamentName = tournamentName;
      state.inSetupProcess = false;
    },
    togglePresenterMode(state) {
      state.isInPresentingMode = !state.isInPresentingMode;
    },
    toogleManualFixtureCreation(state) {
      state.isManualFixtureCreationEnabled = !state.isManualFixtureCreationEnabled;
    },
    setCombinationIntensity(state, intensity) {
      state.combinationIntensity = intensity;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    setReducingIntensity(state, intensity) {
      state.reducingIntensity = intensity;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    setNextMatchGenerationOption(state, option) {
      state.nextMatchGenerationOption = option;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    setMatchComparisonOption(state, option) {
      state.matchComparisonOption = option;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    setReducingDeletionMethod(state, method) {
      state.reducingDeletionMethod = method;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    setReducingComparisonMethod(state, method) {
      state.reducingComparisonMethod = method;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    setReducingMergeMatchFillMethod(state, method) {
      state.reducingMergeMatchFillMethod = method;
      this.dispatch(
        "fixtures/calculatedMatchCombinations",
        { onlySettingsChanged: true },
        { root: true }
      );
    },
    resetState(state) {
      Object.assign(state, initialState);
    }
  },
  actions: {
    adjustTeamsPerMatch({ getters, commit, rootGetters }) {
      if (rootGetters["teams/teamAmount"] == getters["teamsPerMatch"])
        commit("decreaseTeamsPerMatchByOne");
    },
    setState({ commit }, settings) {
      commit("changeTeamsPerMatch", settings.teamsPerMatch);
      commit("setCombinationIntensity", settings.combinationIntensity);
      commit(
        "setNextMatchGenerationOption",
        settings.nextMatchGenerationOption
      );
      commit("setMatchComparisonOption", settings.matchComparisonOption);
      commit("setReducingIntensity", settings.reducingIntensity);
      commit("setReducingDeletionMethod", settings.reducingDeletionMethod);
      commit("setReducingComparisonMethod", settings.reducingComparisonMethod);
      commit(
        "setReducingMergeMatchFillMethod",
        settings.reducingMergeMatchFillMethod
      );
      commit("finishSetupProcess", settings.tournamentName);
    }
  }
};

export default settingsModule;
